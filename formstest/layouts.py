from pyramid.decorator import reify
from pyramid.renderers import get_renderer

class Layouts(object):
    
    @reify
    def base_template(self):
        renderer = get_renderer("templates/base.pt")
        return renderer.implementation().macros['layout']
    
    @classmethod
    def page_title(self):
        return "SHM Customer Relationship Software"
    
class Dashboard(object):
    @reify
    def dashboard_template(self):
        renderer = get_renderer('templates/dashboard.pt')
        return renderer.implementation().macros['dashboard']