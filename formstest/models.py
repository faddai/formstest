from sqlalchemy import (
    Column,
    Integer,
    Text,
    Date,
    String,
    )

from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy.orm import (
    scoped_session,
    sessionmaker,
    )

from zope.sqlalchemy import ZopeTransactionExtension

DBSession = scoped_session(sessionmaker(extension=ZopeTransactionExtension()))
Base = declarative_base()


class MyModel(Base):
    __tablename__ = 'models'
    id = Column(Integer, primary_key=True)
    name = Column(Text, unique=True)
    value = Column(Integer)

    def __init__(self, name, value):
        self.name = name
        self.value = value

class User(Base):
    __tablename__ = 'users'
    __name__ = None
    
    id = Column(Integer, primary_key=True)
    username = Column(String(20), nullable=False)
    firstname = Column(String(60), nullable=False)
    lastname = Column(String(60), nullable=False)
    dob = Column(Date(), nullable=True)
    
    def __init__(self,
		 username=u'',
		 firstname=u'',
		 lastname=u'',
		 dob=None):
	self.username = username
	self.firstname = firstname
	self.lastname = lastname
	self.dob = dob
	
    def __repr__(self):
	return "<User(%r %r)>" % (self.firstname, self.lastname)
	