from pyramid.config import Configurator
from pyramid_beaker import session_factory_from_settings

from sqlalchemy import engine_from_config

from .models import (
    DBSession,
    Base,
    )


"""Merge back to the db session data from application.
It could be a newly created resource.
Merge it into session to make it ready to persist."""
def merge_session_with_post(session, post):
    for key,value in post:
        setattr(session, key, value)
    return session

"""Format data records from DB to application structure
def a_record_to_appstruct(record):
    return dict([(k, record.__dict__[k]) for k in sorted(record.__dict__) if '_sa_' != k[:4]])
"""
def main(global_config, **settings):
    
    from .views import UserEditPage, UserAddPage
    from .models import User

    """ This function returns a Pyramid WSGI application.
    """
    engine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)
    Base.metadata.bind = engine
    config = Configurator(settings=settings)
    config.add_static_view('static', 'static', cache_max_age=3600)
    
    # sessions
    session_factory = session_factory_from_settings(settings)
    config.set_session_factory(session_factory)
    
    # add views
    config.add_view(UserAddPage, route_name='new_user', renderer='templates/users/edit.pt')
    config.add_view(UserEditPage, route_name='edit_user', renderer='templates/users/edit.pt')
    
    # add routes
    config.add_route('home', '/')
    config.add_route('view_users', '/users')
    config.add_route('view_user', '/users/view/{user_id}')
    config.add_route('edit_user', '/users/edit/{user_id}')
    config.add_route('new_user', '/users/new')
    config.add_route('login', '/users/login')
    #config.add_route('index_page', '/dashboard/index')
   
    
    config.scan()
    return config.make_wsgi_app()
