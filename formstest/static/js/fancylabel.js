/*
#
# fancylabel.jquery.js
#
# by Ludwig Pettersson
# <http://luddep.se>
#

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
*/

(function($){

    var speed = 80,
        startColor,
        fadedColor;

    // Sometimes the first color animation wont fire, so we fire off one.
    //$('html').animate({'backgroundColor': $('html').css('background-color')}, 1);

    $.fn.fancyLabel = function(_fadedColor){

        $('span', this).each(function() {

            var p = $(this).parent();
            var span = $(this);
            p.addClass('fancy-label');

            var input = $('input', span),
                label = $('label', span);

            startColor = label.css('color');
            fadedColor = _fadedColor ? _fadedColor : "#eee";

            // Safari takes a few ms to actually set focus on the input
            label.click(function() {
                input.focus();
                return false;
            });

            var showLabel = function() {
                label
                    .show(0)
                    //.animate({'color': startColor}, speed);
            };

            var keyupTimeout = null;

            // HUGE HACK ALERT: try to make autofill work decently
            setTimeout(function(){
              if (input.val() != "")
                  label.hide(0);
            }, 10)
            setTimeout(function(){
              if (input.val() != "")
                  label.hide(0);
            }, 100)
            setTimeout(function(){
              if (input.val() != "")
                  label.hide(0);
            }, 300)

            input
                .focus(function() {
                    //label.animate({'color': fadedColor}, speed);
                })
                .keydown(function(event) {

                    // Timeout because the event isn't propogated
                    // to the actual DOM element until a bit after
                    // this method gets called.
                    setTimeout(function() {
                        if (input.val().length)
                            label.hide(0);
                    }, 0);

                    /*
                    if (keyupTimeout)
                        window.clearTimeout(keyupTimeout);

                    keyupTimeout = window.setTimeout(function(element) {
                        if (element.val().length === 0)
                            showLabel();
                    }, 0, $(this));
                    */
                })
                .keyup(function() {
                    if (keyupTimeout)
                        window.clearTimeout(keyupTimeout);

                    keyupTimeout = window.setTimeout(function(element) {
                        if (element && element.html() && element.html().length === 0)
                            showLabel();
                    }, 0, $(this));
                })
                .blur(function(event) {
                    if (/*event.which !== 8 && */!$(this).val() || $(this).val().length === 0)
                    {
                        showLabel();
                    }
                });
        });
    }

})(jQuery);
